#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Text::Nyarkdown;

my $nd = Text::Nyarkdown->new( use_metadata => 0 );

subtest 'gist' => sub {
    is(
        $nd->markdown(q{gist:nyarla@4135424}, { enable_embed => ['Gist'] }),
        qq{<script src="https://gist.github.com/nyarla/4135424.js"></script>\n},
    );

    done_testing;
};

subtest 'youtube' => sub {
    is(
        $nd->markdown(q{YouTube:OfMJkVd6Ffg:600x400:},
                      { enable_embed => ['YouTube'] }  ),
        qq{<div class="youtube embed">
<iframe width="600" height="400"
        src="https://www.youtube-nocookie.com/embed/OfMJkVd6Ffg?rel=0"
        frameborder="0" allowfullscreen></iframe>
</div>\n}
        );

    done_testing;
};

done_testing;

