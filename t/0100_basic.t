#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Test::More;

use Text::Nyarkdown qw/markdown/;

subtest 'ruby (group)' => sub {
    is(
        markdown(q<{約束されし勝利の剣|エクスカリバー}>),
        qq{<p><ruby>約束されし勝利の剣<rt>エクスカリバー</rt></ruby></p>\n},
    );

    done_testing;
};

subtest 'ruby (single)' => sub {
    is(
        markdown(q<{電子書籍|でん|し|しょ|せき}>),
        qq{<p><ruby>電<rt>でん</rt>子<rt>し</rt>書<rt>しょ</rt>籍<rt>せき</rt></ruby></p>\n}
    );

    done_testing;
};

subtest 'tcy' => sub {
    is(
        markdown(q<昭和^63^年>),
        qq{<p>昭和<span class="tcy">63</span>年</p>\n}
    );

    is(
        markdown(q{^3\^5^}),
        qq{<p><span class="tcy">3^5</span></p>\n}
    );

    done_testing;
};

done_testing;

