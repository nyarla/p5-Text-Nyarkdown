# core
requires 'Text::MultiMarkdown'      => '>= 1.000034';
requires 'Class::Method::Modifiers' => '>= 2.03';
requires 'Class::Load'              => '>= 0.20';
requires 'Digest::MD5'              => '>= 2.52';

on 'test' => sub {
    requires 'Test::More' => '>= 0.98';
};

