package Text::Nyarkdown;

use strict;
use warnings;

use parent qw/ Text::MultiMarkdown /;

our $VERSION = '0.000001';

use Class::Method::Modifiers;
use Class::Load qw/ load_class is_class_loaded /;
use Digest::MD5 qw/ md5_hex /;

our @EXPORT_OK = qw/markdown/;

$Text::Markdown::g_escape_table{'^'} = md5_hex('^');
$Text::Markdown::g_escape_table{'|'} = md5_hex('|');

# copied from Text::MultiMarkdown
sub markdown {
    my ( $self, $text, $options ) = @_;

    # Detect functional mode, and create an instance for this run..
    unless (ref $self) {
        if ( $self ne __PACKAGE__ ) {
            my $ob = __PACKAGE__->new();
                                # $self is text, $text is options
            return $ob->markdown($self, $text);
        }
        else {
            croak('Calling ' . $self . '->markdown (as a class method) is not supported.');
        }
    }

    $options ||= {};

    %$self = (%{ $self->{params} }, %$options, params => $self->{params});

    $self->_CleanUpRunData($options);

    return $self->_Markdown($text);
}

around _EncodeBackslashEscapes => sub {
    my $orig = shift;

    local $_ = $orig->(@_);

    s! \\\^ !$Text::Markdown::g_escape_table{'^'}!ogx;
    s! \\\| !$Text::Markdown::g_escape_table{'|'}!ogx;

    return $_;
};

around new => sub {
    my $orig  = shift;
    my $class = shift;
    my %p     = @_;

    $p{'disable_ruby'} ||= 0;
    $p{'disable_tcy'}  ||= 0;

    return $orig->( $class, %p );
};

around _RunBlockGamut => sub {
    my $orig = shift;
    my $self = shift;
    my $text = shift;
    my $opts = shift;

    $text = $self->_DoEmbedPlugins($text, $opts);

    return $orig->( $self, $text, $opts );
};

sub _DoEmbedPlugins {
    my ( $self, $text, $options ) = @_;

    my @plugins = @{ $self->{'enable_embed'} || [] };

    for my $plugin ( @plugins ) {
        my $module = $plugin;
        if ( $module !~ s{[+](.+)}{$1} ) {
            $module = "Text::Nyarkdown::Embed::${module}";
        }

        load_class($module)
            if ( ! is_class_loaded($module) );

        $text = $module->interpolate( $self, $text, $options );
    }

    return $text;
}

around _RunSpanGamut => sub {
    my $orig   = shift;
    my $self   = shift;
    my $source = shift;

    my $text   = $orig->( $self, $source );
       $text   = $self->_DoRubyText($text);
       $text   = $self->_DoTCYText($text);

    return $text;
};

sub _DoRubyText {
    my ( $self, $text ) = @_;
    return $text if ( $self->{'disable_ruby'} );

    $text =~ s![{](.+?)[}]!
        my @captured = split m{[|]}, $1;
        
        my @target   = split q{}, shift @captured;
        my @ruby     = @captured;

        my $rubytext = q{<ruby>};
        if ( @target == @ruby ) {
           for my $str ( @target ) {
              $rubytext .= "${str}<rt>@{[ shift @ruby ]}</rt>";
           }
        }
        else {
           my $str = join q{}, @target; 
           my $rb  = join q{}, @ruby;

           $rubytext .= "${str}<rt>$rb</rt>";
        }
        $rubytext .= "</ruby>";

        $rubytext
    !egsx;

    return $text;
}

sub _DoTCYText {
    my ( $self, $text ) = @_;
    return $text if ( $self->{'disable_tcy'} );

    $text =~ s{(\^)(?=\S)(.+?)(?<=\S)\1}
        {<span class="tcy">$2</span>}gsx;

    return $text;
}

"No Markdown, No Life."

__END__

=encoding utf8

=head1 NAME

Text::Nyarkdown - Extended L<Text::MultiMarkdown>

=head1 SYNOPSIS

  use Text::Nyarkdown qw/markdown/;

  my $text = '...';
  my $marked = markdown($text);

  # ruby text
  my $marked = markdown('{約束されし勝利の剣|エクスカリバー}');
  warn $marked; # => q{<p><ruby>約束されし勝利の剣<rt>エクスカリバー</rt></ruby></p>}

  # TateChuYoko
  my $marked = markdown('昭和^63^年');
  warn $marked; # => q{<p>昭和<span class="tcy">63<span>"}

  # embedded content (gist)
  my $markded = markdown('gist:nyarla@4135424', { enable_embed => ['Gist'] });
  warn $marked; # q{<script src="https://gist.github.com/nyarla/4135424.js"></script>}

=head1 DESCRIPTION

This module is an extended L<Text::MultiMarkdown>.

This module has been extended as follow:

=over

=item Ruby text (DenDenMarkdown-like style)

=item Extension in the vertical layout of Japan (TateChuYokyo [縦中横] marking)

=item Embedded contents (Gist, YouTube, ...etc)

=back

=head1 EXTENDED MARKDOWN OPTIONS

=head2 disable_ruby :Bool

disable ruby text support.

=head2 disable_tcy :Bool

disable TateChuYokyo support.

=head2 enable_embed :ArrayRef[Str]

list of enabled embed extensions.

=head1 EXTENDED MARKDOWN SYNTAX

=head2 Ruby Text

  {約束されし勝利の剣|エクスカリバー}
  => <ruby>約束されし勝利の剣<rt>エクスカリバー</rt></ruby>

  {電子書籍|でん|し|しょ|せき}
  => <ruby>電<rt>でん</rt>子<rt>し</rt>書<rt>しょ</rt>籍</rt>せき</rt></ruby>

=head2 TateChuYoko

  昭和^63^年
  => 昭和<span class="tcy">63</span>年

=head1 EXTENSIONS FOR EMBEDDED CONTENT

=over

=item L<Text::Nyarkdown::Embed::Gist>

=item L<Text::Nyarkdown::Embed::YouTube>

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

License of this module is to inherit L<Text::MultiMarkdown>.

So, license of this module is The (three-clause) BSD License.

=head1 SEE ALSO

L<Text::Markdown>, L<Text::MultiMarkdown>

About License: L<http://www.opensource.org/licenses/bsd-license.php>
