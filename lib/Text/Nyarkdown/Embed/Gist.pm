package Text::Nyarkdown::Embed::Gist;

use strict;
use warnings;

our $SYNTAX   = qr{^gist:([^@]+?)[@](\d+)};
our $BASE_URL = 'https://gist.github.com';

sub interpolate {
    my ( $class, $nyark, $text, $options ) = @_;

    $text =~ s{$SYNTAX}{<script src="${BASE_URL}/$1/$2.js"></script>}gs;

    return $text;
}

"No Gist, No Life"

__END__

=head1 NAME

Text::Nyarkdown::Embed::Gist - Gist extension for Text::Nyarkdown

=head1 EXTENDED MARKDWON SYNTAX

  gist:nyarla@4135424
  => <script src="https://gist.github.com/nyarla/4135424.js"></script>

=head1 AUTHOR

Naoki Okamura (Nyarla) E<gt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

The (three-clause) BSD License.

=head1 SEE ALSO

L<Text::Nyarkdown>

L<https://gist.github.com/>

=cut

