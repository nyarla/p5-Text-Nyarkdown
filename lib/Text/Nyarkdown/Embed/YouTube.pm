package Text::Nyarkdown::Embed::YouTube;

use strict;
use warnings;

our $BASE_URL     = "https://www.youtube-nocookie.com/embed";
our $DEFAULT_SIZE = "560x315";
our $SYNTAX       = qr{^YouTube:([a-zA-Z0-9]+)(?:[:](\d+[x]\d+)[:])?\s*};

sub interpolate {
    my ( $class, $nyark, $text, $options ) = @_;

    $text =~ s{$SYNTAX}{
        my $movie_id   = $1;
        my $movie_size = $2 || $DEFAULT_SIZE;

        my ( $width, $height ) = split m{x}, $movie_size;

        qq{
<div class="youtube embed">
<iframe width="${width}" height="${height}"
        src="${BASE_URL}/${movie_id}?rel=0"
        frameborder="0" allowfullscreen></iframe>
</div>};
    }egsx;

    return $text;
}

"Let's Enjoy Movie!"

__END__

=head1 NAME

Text::Nyarkdown::Embed::YouTube - YouTube extension for Text::Nyarkdown

=head1 EXTENDED MARKDOWN SYNTAX

  YouTube:OfMJkVd6Ffg
  => <div class="youtube embed">
     <iframe width="560" height="315"
             src="https://www.youtube-nocookie.com/embed/OfMJkVd6Ffg?rel=0"
             frameborder="0" allowfullscreen></iframe>
     </div>
    

  YouTube:OfMJkVd6Ffg:600x400:
  => <div class="youtube embed">
     <iframe width="600" height="400"
             src="https://www.youtube-nocookie.com/embed/OfMJkVd6Ffg?rel=0"
             frameborder="0" allowfullscreen></iframe>
     </div>
    
=head1 GLOBAL OPTIONS

=head2 C<$Text::Nyarkdown::Embed::YouTube::BASE_URL>

Base URL for YouTube embedding link

Default value: C<"https://www.youtube-nocookie.com/embed>

=head2 C<$Text::Nyarkdown::Embed::YouTube::DEFAULT_SIZE>

Default size for movie player.

Default value: C<560x315>

=head1 AUTHOR

Naoki Okamura (Nyarla) E<gt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

The (three-clause) BSD License.

=head1 SEE ALSO

L<Text::Nyarkdown>

L<https://www.youtube.com/>

=cut

